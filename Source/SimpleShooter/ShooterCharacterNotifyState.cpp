// Fill out your copyright notice in the Description page of Project Settings.


#include "ShooterCharacterNotifyState.h"
#include "Kismet/KismetSystemLibrary.h"
#include "Engine/CollisionProfile.h"

void UShooterCharacterNotifyState::NotifyBegin(USkeletalMeshComponent* MeshComp, UAnimSequenceBase* Animation, float FrameDeltaTime)
{
	ActorsToIgnore.Add(MeshComp->GetOwner());
}

void UShooterCharacterNotifyState::NotifyTick(USkeletalMeshComponent* MeshComp, UAnimSequenceBase* Animation, float FrameDeltaTime)
{

	FVector StartLocation = MeshComp->GetSocketLocation("lowerarm_r");
	FVector EndLocation = MeshComp->GetSocketLocation("Muzzle_01");
	
	FHitResult Hit;
	
	bool bSuccess = UKismetSystemLibrary::SphereTraceSingle(MeshComp->GetWorld(), StartLocation, EndLocation, 10, UEngineTypes::ConvertToTraceType(ECC_GameTraceChannel2), false, ActorsToIgnore, EDrawDebugTrace::None, Hit, true);
	
	if (bSuccess)
	{
		AActor* HitActor = Hit.GetActor();
		if (HitActor != nullptr)
		{
			ActorsToIgnore.Add(HitActor);
			UE_LOG(LogTemp, Warning, TEXT("Object Hit is: %s"), *HitActor->GetName());
			int Damage = 10;
			AActor* Owner = MeshComp->GetOwner();
			AController* OwnerController = GetOwnerController(MeshComp);
			FRotator ControllerRotation;
			FVector ControllerLocation;
			UE_LOG(LogTemp, Warning, TEXT("The Controller is: %s"), *Owner->GetName());
			OwnerController->GetPlayerViewPoint(ControllerLocation, ControllerRotation); //Causing a crash...
			FVector ShotDirection = -ControllerRotation.Vector();
			FPointDamageEvent DamageEvent(Damage, Hit, ShotDirection, nullptr);
			HitActor->TakeDamage(Damage, DamageEvent, OwnerController, Owner);
			UE_LOG(LogTemp, Warning, TEXT("The Target is: %s"), *HitActor->GetName());

			UE_LOG(LogTemp, Warning, TEXT("The Damage was: %d"), Damage);
		}

	}

}

AController* UShooterCharacterNotifyState::GetOwnerController(USkeletalMeshComponent* MeshComp) const
{
	APawn* OwnerPawn = Cast<APawn>(MeshComp->GetOwner());

	if (OwnerPawn == nullptr) return nullptr;

	return  OwnerPawn->GetController();
}

void UShooterCharacterNotifyState::NotifyEnd(USkeletalMeshComponent* MeshComp, UAnimSequenceBase* Animation)
{
	ActorsToIgnore.Empty();
}