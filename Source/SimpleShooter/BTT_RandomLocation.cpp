// Fill out your copyright notice in the Description page of Project Settings.


#include "BTT_RandomLocation.h"
#include "BehaviorTree/BlackboardComponent.h"
#include "AIController.h"
#include "ShooterCharacter.h"
#include "NavigationSystem.h"

UBTT_RandomLocation::UBTT_RandomLocation()
{
	NodeName = TEXT("Random Location");
}
EBTNodeResult::Type UBTT_RandomLocation::ExecuteTask(UBehaviorTreeComponent& OwnerComp, uint8* NodeMemory)
{
	Super::ExecuteTask(OwnerComp, NodeMemory);
	if (OwnerComp.GetAIOwner() == nullptr)
	{
		return EBTNodeResult::Failed;
	}
	AShooterCharacter* Character = Cast<AShooterCharacter>(OwnerComp.GetAIOwner()->GetPawn());
	if (Character == nullptr)
	{
		return EBTNodeResult::Failed;
	}
	UNavigationSystemV1* NavSys = UNavigationSystemV1::GetCurrent(GetWorld());
	if (!NavSys) 
	{
		return EBTNodeResult::Failed;
	}
	FVector CharacterLocation = Character->GetActorLocation();
	FNavLocation ResultLocation;
	if (NavSys->GetRandomReachablePointInRadius(CharacterLocation, RandLocRadius, ResultLocation))
	{
		OwnerComp.GetBlackboardComponent()->SetValueAsVector(TEXT("PatrolLocation"), ResultLocation.Location);
	}
	else
	{
		OwnerComp.GetBlackboardComponent()->SetValueAsVector(TEXT("PatrolLocation"), CharacterLocation);
	}
	return EBTNodeResult::Succeeded;
}