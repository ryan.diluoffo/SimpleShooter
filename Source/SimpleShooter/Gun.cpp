// Fill out your copyright notice in the Description page of Project Settings.


#include "Gun.h"
#include "Components/SkeletalMeshComponent.h"
#include "Kismet\GameplayStatics.h"
#include "DrawDebugHelpers.h"
#include "Perception\AISense_Hearing.h"
#include "TimerManager.h"

// Sets default values
AGun::AGun()
{
	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	Root = CreateDefaultSubobject<USceneComponent>(TEXT("Root"));
	SetRootComponent(Root);

	Mesh = CreateDefaultSubobject<USkeletalMeshComponent>(TEXT("Mesh"));
	Mesh->SetupAttachment(Root);
}

// Called when the game starts or when spawned
void AGun::BeginPlay()
{
	Super::BeginPlay();
	
}

// Called every frame
void AGun::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

void AGun::PullTrigger()
{
	if (Clip <= 0) return; //add empty clip sound
	UGameplayStatics::SpawnEmitterAttached(MuzzleFlash, Mesh, TEXT("MuzzleFlashSocket"));
	UGameplayStatics::SpawnSoundAttached(MuzzleSound, Mesh, TEXT("MuzzleFlashSocket"));
	FHitResult Hit; 
	FVector ShotDirection; 
	
		bool bSuccess = GunTrace(Hit, ShotDirection);
		if (bSuccess)
		{
			Damage = BaseDamage; 
			UGameplayStatics::SpawnEmitterAtLocation(GetWorld(), ImpactEffect, Hit.Location, ShotDirection.Rotation());
			UGameplayStatics::PlaySoundAtLocation(GetWorld(), ImpactSound, Hit.Location);
			UAISense_Hearing::ReportNoiseEvent(GetWorld(), Hit.Location, 1.0, this, 500, TEXT("Shot Fired"));
			Clip--;
			AActor* HitActor = Hit.GetActor();
			if (HitActor != nullptr)
			{
				UE_LOG(LogTemp, Warning, TEXT("Object hit is: %s"), *HitActor->GetName());
				UE_LOG(LogTemp, Warning, TEXT("Object Component hit is: %s"), *Hit.GetComponent()->GetName());
				FName Bone = Hit.BoneName;
				if (Bone == "head") {
					Damage = 2 * BaseDamage;
				UE_LOG(LogTemp, Warning, TEXT("Headshot on: %s"), *Bone.ToString());
				}

				AController* OwnerController = GetOwnerController();
				FPointDamageEvent DamageEvent(Damage, Hit, ShotDirection, nullptr);
				HitActor->TakeDamage(Damage, DamageEvent, OwnerController, this);
				UE_LOG(LogTemp, Warning, TEXT("The Damage was: %d"), Damage);
			}
			GetWorldTimerManager().SetTimer(ShotHandle, this, &AGun::PullTrigger, RateOfFire, bAutoFire);
		}
	
}

bool AGun::GunTrace(FHitResult& Hit, FVector& ShotDirection)
{
	AController* OwnerController = GetOwnerController();
	if (OwnerController == nullptr) return false;
	 
	FVector ControllerLocation;
	FRotator ControllerRotation;
	OwnerController->GetPlayerViewPoint(ControllerLocation, ControllerRotation);
	ShotDirection = -ControllerRotation.Vector();

	FVector End = ControllerLocation + ControllerRotation.Vector() * MaxRange;
	FCollisionQueryParams Params;
	Params.AddIgnoredActor(this);
	Params.AddIgnoredActor(GetOwner());
	
	return GetWorld()->LineTraceSingleByChannel(Hit, ControllerLocation, End, ECollisionChannel::ECC_GameTraceChannel1, Params);

}

AController* AGun::GetOwnerController() const
{
	APawn* OwnerPawn = Cast<APawn>(GetOwner());
	if (OwnerPawn == nullptr) return nullptr;

	return  OwnerPawn->GetController();
}

void AGun::Reload() 
{
	//Play Reload Sound and Animation
	GetWorldTimerManager().SetTimer(ReloadHandle, this, &AGun::ReloadAmmo, ReloadTime, false);
}
void AGun::ReloadAmmo()
{
	float ReserveAmmo = CurrentAmmo;
	CurrentAmmo = fmax((CurrentAmmo - MaxClip) + Clip, 0);
	Clip = fmin(MaxClip, ReserveAmmo + Clip);
}
float AGun::GetCurrentAmmo()
{
	return CurrentAmmo;
}

float AGun::GetClip()
{
	return Clip;
}

void AGun::ReleaseTrigger()
{
	GetWorldTimerManager().ClearTimer(ShotHandle);
}

UTexture2D* AGun::GetHUDImage()
{
	return HUDImage;
}