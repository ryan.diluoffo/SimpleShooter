// Fill out your copyright notice in the Description page of Project Settings.


#include "BTTask_CleaBlackboardValue.h"
#include "BehaviorTree/BlackboardComponent.h"
UBTTask_CleaBlackboardValue::UBTTask_CleaBlackboardValue()
{
	NodeName = TEXT("Clear Blackboard Value");
}
EBTNodeResult::Type UBTTask_CleaBlackboardValue::ExecuteTask(UBehaviorTreeComponent& OwnerComp, uint8* NodeMemory)
{
	Super::ExecuteTask(OwnerComp, NodeMemory);
	
	OwnerComp.GetBlackboardComponent()->ClearValue(GetSelectedBlackboardKey());
	return EBTNodeResult::Succeeded;
}