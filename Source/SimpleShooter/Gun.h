// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "Engine/Texture2D.h"
#include "Gun.generated.h"

UCLASS()
class SIMPLESHOOTER_API AGun : public AActor
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	AGun();
	// Called every frame
	virtual void Tick(float DeltaTime) override;

	void PullTrigger(); 
	void Reload();
	void ReloadAmmo();
	UFUNCTION(BlueprintPure)
	float GetCurrentAmmo();
	UFUNCTION(BlueprintPure)
	float GetClip();
	UFUNCTION(BlueprintPure)
		UTexture2D* GetHUDImage();
	UFUNCTION()
	void ReleaseTrigger();
protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

private:
	UPROPERTY(VisibleAnywhere)
		USceneComponent* Root; 
	UPROPERTY(VisibleAnywhere)
		USkeletalMeshComponent* Mesh;
	UPROPERTY(EditAnywhere)
		UParticleSystem* MuzzleFlash; 
	UPROPERTY(EditAnywhere)
		USoundBase* MuzzleSound; 
	UPROPERTY(EditAnywhere)
		UParticleSystem* ImpactEffect;
	UPROPERTY(EditAnywhere)
		USoundBase* ImpactSound;
	UPROPERTY(EditAnywhere)
		float MaxRange = 1000.0f;
	UPROPERTY(EditAnywhere)
		float BaseDamage = 10.0f;
	UPROPERTY(EditAnywhere)
		float Damage = 10.0f;
	

	UPROPERTY(EditAnywhere)
		float MaxClip = 10.0f; 
	UPROPERTY(EditAnywhere)
		float Clip = 10.0f; 
	UPROPERTY(EditAnywhere)
		float MaxAmmo = 100.0f; 
	UPROPERTY(EditAnywhere)
		float CurrentAmmo = 100.0f;

	UPROPERTY(EditAnywhere)
		float RateOfFire = 0.0f;
	UPROPERTY(EditAnywhere)
		bool bAutoFire = false;
	UPROPERTY(EditAnywhere)
		float ReloadTime = 1.0f;
	UPROPERTY(EditAnywhere)
		UTexture2D* HUDImage;
	
	FTimerHandle ShotHandle;
	FTimerHandle ReloadHandle;

	bool GunTrace(FHitResult& Hit, FVector& ShotDirection);

	AController* GetOwnerController() const; 
};
