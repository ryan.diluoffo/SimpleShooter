// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Character.h"
#include "Camera/CameraComponent.h"
#include "ShooterCharacter.generated.h"

class AGun;
class AConsoleTrigger;

UCLASS()
class SIMPLESHOOTER_API AShooterCharacter : public ACharacter
{
	GENERATED_BODY()

public:
	// Sets default values for this character's properties
	AShooterCharacter();
	UFUNCTION()
		void OnOverlapBegin(class UPrimitiveComponent* OverlappedComp, class AActor* OtherActor, class UPrimitiveComponent* OtherComp, int32 OtherBodyIndex, bool bFromSweep, const FHitResult& SweepResult);
	UFUNCTION()
		void OnOverlapEnd(class UPrimitiveComponent* OverlappedComp, class AActor* OtherActor, class UPrimitiveComponent* OtherComp, int32 OtherBodyIndex);
protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

public:	
	UFUNCTION(BlueprintPure)
		bool IsDead() const; 
	UFUNCTION(Blueprintpure)
		bool GetIsAttacking(); 
	UFUNCTION(BlueprintCallable)
		void SetIsAttacking(bool State);
	UFUNCTION(BlueprintPure)
		float GetHealthPercent() const;
	UFUNCTION(BlueprintPure)
		AGun* GetActiveGun();
	UFUNCTION(BlueprintPure)
		AGun* GetHolsteredGun();
	UFUNCTION()
		FName GetCritSpot();

	// Called every frame
	virtual void Tick(float DeltaTime) override;

	// Called to bind functionality to input
	virtual void SetupPlayerInputComponent(class UInputComponent* PlayerInputComponent) override;
	
	virtual float TakeDamage(float DamageAmount, struct FDamageEvent const& DamageEvent, class AController *Eventinstigator, AActor *DamageCauser) override; 
	void Shoot();
	void Release();
	void ReloadGun();
	void SwitchGun(); 
	void MeleeAttack();
	void ZoomIn();
	void ZoomOut();
	void Interact();
private:
	void MoveForward (float AxisValue);
	void MoveRight (float AxisValue);
	void LookUpRate(float AxisValue);
	void LookRightRate(float AxisValue);
	UPROPERTY(EditAnywhere)
		float RotationRate = 70.0f;

	UPROPERTY(EditDefaultsOnly)
		TArray<TSubclassOf<AGun>> GunClass;

	UPROPERTY()
		AGun* ActiveGun;
	UPROPERTY()
		AGun* HolsteredGun;

	UPROPERTY(EditDefaultsOnly)
		float MaxHealth = 100;
	UPROPERTY(VisibleAnywhere)
		float Health; 
	UPROPERTY(EditDefaultsOnly)
		bool bIsDead; 
	UPROPERTY(EditAnywhere)
		FName CritSpot = "";

	UPROPERTY(EditDefaultsOnly)
		bool bIsAttacking;
	float CurrentWeaponNum = 0; 

	UPROPERTY(EditAnywhere)
		UCameraComponent* MainCamera;
	UPROPERTY(EditAnywhere)
		UCameraComponent* ADSCamera;

	UPROPERTY(EditAnywhere)
		class UWidgetComponent* HealthWidget;

	UPROPERTY(EditAnywhere)
		class UCapsuleComponent* CapsuleComp;
	UPROPERTY()
		AConsoleTrigger* Console;
};
