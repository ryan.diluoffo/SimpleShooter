// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "Components/BoxComponent.h"
#include "Components/WidgetComponent.h"
#include "ConsoleTrigger.generated.h"

UCLASS()
class SIMPLESHOOTER_API AConsoleTrigger : public AActor
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	AConsoleTrigger();
	UFUNCTION()
		void OnOverlapBegin(class UPrimitiveComponent* OverlappedComp, class AActor* OtherActor, class UPrimitiveComponent* OtherComp, int32 OtherBodyIndex, bool bFromSweep, const FHitResult& SweepResult);
	UFUNCTION()
		void OnOverlapEnd(class UPrimitiveComponent* OverlappedComp, class AActor* OtherActor, class UPrimitiveComponent* OtherComp, int32 OtherBodyIndex); 
	void OpenDoor();
	bool bIsInteractble = true; 
protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;
private:
	
	UPROPERTY(EditAnyWhere)
		class UBoxComponent* Console = nullptr;
	UPROPERTY(EditAnywhere)
		TSubclassOf<class UUserWidget> InteractWidget;
	UPROPERTY()
		UUserWidget* Interact; 
	UPROPERTY(EditAnywhere)
		AActor* Door; 
	bool bIsColliding = false; 
	bool bDestroy = false; 
};
