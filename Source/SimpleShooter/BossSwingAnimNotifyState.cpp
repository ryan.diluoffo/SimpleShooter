// Fill out your copyright notice in the Description page of Project Settings.


#include "BossSwingAnimNotifyState.h"
#include "Kismet/KismetSystemLibrary.h"
#include "Engine/CollisionProfile.h"


void UBossSwingAnimNotifyState::NotifyBegin(USkeletalMeshComponent* MeshComp, UAnimSequenceBase* Animation, float FrameDeltaTime)
{
	ActorsToIgnore.Add(MeshComp->GetOwner());
}

void UBossSwingAnimNotifyState::NotifyTick(USkeletalMeshComponent* MeshComp, UAnimSequenceBase* Animation, float FrameDeltaTime)
{
	FVector StartLocationR = MeshComp->GetSocketLocation("weapon_r");
	FVector EndLocationR = MeshComp->GetSocketLocation("FX_Trail_R_01");
	FVector StartLocationL = MeshComp->GetSocketLocation("weapon_l");
	FVector EndLocationL = MeshComp->GetSocketLocation("FX_Trail_L_01");
	FHitResult Hit;

	bool bSuccessR = UKismetSystemLibrary::SphereTraceSingle(MeshComp->GetWorld(), StartLocationR, EndLocationR, 20, UEngineTypes::ConvertToTraceType(ECC_GameTraceChannel2), false, ActorsToIgnore, EDrawDebugTrace::None, Hit, true);
	bool bSuccessL = UKismetSystemLibrary::SphereTraceSingle(MeshComp->GetWorld(), StartLocationL, EndLocationL, 20, UEngineTypes::ConvertToTraceType(ECC_GameTraceChannel2), false, ActorsToIgnore, EDrawDebugTrace::None, Hit, true);

	if(bSuccessR || bSuccessL)
	{
	AActor* HitActor = Hit.GetActor();
		if(HitActor != nullptr)
		{
			ActorsToIgnore.Add(HitActor);
			int Damage = 30; 
			AActor* Owner = MeshComp->GetOwner();
			AController* OwnerController = GetOwnerController(MeshComp);
			FRotator ControllerRotation;
			FVector ControllerLocation; 
			OwnerController->GetPlayerViewPoint(ControllerLocation, ControllerRotation);
			FVector ShotDirection = -ControllerRotation.Vector();
			FPointDamageEvent DamageEvent(Damage, Hit, ShotDirection, nullptr);
			HitActor->TakeDamage(Damage, DamageEvent, OwnerController, Owner);
			UE_LOG(LogTemp, Warning, TEXT("The Damage was: %d"), Damage);
		}
		
	}

}

AController* UBossSwingAnimNotifyState::GetOwnerController(USkeletalMeshComponent* MeshComp) const
{
	APawn* OwnerPawn = Cast<APawn>(MeshComp->GetOwner());
	if (OwnerPawn == nullptr) return nullptr;

	return  OwnerPawn->GetController();
}
	
void UBossSwingAnimNotifyState::NotifyEnd(USkeletalMeshComponent* MeshComp, UAnimSequenceBase* Animation)
{
	ActorsToIgnore.Empty();
}

