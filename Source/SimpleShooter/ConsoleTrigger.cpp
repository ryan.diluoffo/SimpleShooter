// Fill out your copyright notice in the Description page of Project Settings.


#include "ConsoleTrigger.h"
#include "Blueprint/UserWidget.h"

// Sets default values
AConsoleTrigger::AConsoleTrigger()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;
	Console = CreateDefaultSubobject<UBoxComponent>(TEXT("ConsoleCollider"));
	Console->SetupAttachment(RootComponent);
	Console->SetRelativeScale3D(FVector(5.0f, 3.0f, 5.0f));
	Console->SetCollisionResponseToChannel(ECollisionChannel::ECC_GameTraceChannel1, ECollisionResponse::ECR_Ignore);
	Console->OnComponentBeginOverlap.AddDynamic(this, &AConsoleTrigger::OnOverlapBegin);
	Console->OnComponentEndOverlap.AddDynamic(this, &AConsoleTrigger::OnOverlapEnd);
}

// Called when the game starts or when spawned
void AConsoleTrigger::BeginPlay()
{
	Super::BeginPlay();
	
	
}

// Called every frame
void AConsoleTrigger::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);
}

void AConsoleTrigger::OnOverlapBegin(class UPrimitiveComponent* OverlappedComp, class AActor* OtherActor, class UPrimitiveComponent* OtherComp, int32 OtherBodyIndex, bool bFromSweep, const FHitResult& SweepResult)
{
	if (bDestroy && OtherActor == GetWorld()->GetFirstPlayerController()->GetPawn())
	{
		this->Destroy(); 
		return;
	}
	if (OtherActor != GetWorld()->GetFirstPlayerController()->GetPawn()) return;
		Interact = CreateWidget(GetWorld()->GetFirstPlayerController(), InteractWidget);
		if (Interact != nullptr )
		{
			Interact->AddToViewport();
			bIsColliding = true; 
		}
	
}
void AConsoleTrigger::OnOverlapEnd(class UPrimitiveComponent* OverlappedComp, class AActor* OtherActor, class UPrimitiveComponent* OtherComp, int32 OtherBodyIndex)
{
	
	if (OtherActor != GetWorld()->GetFirstPlayerController()->GetPawn() && OtherActor != this) return;
	if (Interact != nullptr)
	{
		Interact->RemoveFromViewport();
		bIsColliding = false; 
	}
}

void AConsoleTrigger::OpenDoor()
{
	if (bIsColliding && Door != nullptr)
	{
		
		Door->Destroy();
		FVector NewLocation(2355.0, -2900.0, 190.0);
		this->SetActorLocation(NewLocation, false, nullptr, ETeleportType::ResetPhysics);
		bDestroy = true; 
	}
}