// Fill out your copyright notice in the Description page of Project Settings.


#include "ShooterCharacter.h"
#include "Gun.h"
#include "ConsoleTrigger.h"
#include "Components/CapsuleComponent.h"
#include "SimpleShooterGameModeBase.h"
#include "Components/WidgetComponent.h"

// Sets default values
AShooterCharacter::AShooterCharacter()
{
 	// Set this character to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;
	TArray<UCapsuleComponent*> Capsule;
	GetComponents<UCapsuleComponent>(Capsule);
	CapsuleComp = Capsule[0];
	CapsuleComp->OnComponentBeginOverlap.AddDynamic(this, &AShooterCharacter::OnOverlapBegin);
	CapsuleComp->OnComponentEndOverlap.AddDynamic(this, &AShooterCharacter::OnOverlapEnd);
	Console = nullptr; 
}

// Called when the game starts or when spawned
void AShooterCharacter::BeginPlay()
{
	Super::BeginPlay();

	Health = MaxHealth;


	ActiveGun = GetWorld()->SpawnActor<AGun>(GunClass[CurrentWeaponNum]);
	ActiveGun->AttachToComponent(GetMesh(), FAttachmentTransformRules::KeepRelativeTransform, TEXT("WeaponSocket"));
	ActiveGun->SetOwner(this);

	HolsteredGun = GetWorld()->SpawnActor<AGun>(GunClass[CurrentWeaponNum + 1]);
	HolsteredGun->AttachToComponent(GetMesh(), FAttachmentTransformRules::KeepRelativeTransform, TEXT("WeaponSocket"));
	HolsteredGun->SetOwner(this);
	HolsteredGun->SetActorHiddenInGame(true);

	if (!GetController()->GetName().Contains("BP_BossAIController")) {
		GetMesh()->HideBoneByName(TEXT("weapon_r"), EPhysBodyOp::PBO_None);
	}

	if (GetController()->GetName().Contains("BP_BossAIController")) {
		ActiveGun->SetActorHiddenInGame(true);
	}

	TArray<UWidgetComponent*> WidgetComp;
	GetComponents<UWidgetComponent>(WidgetComp);
	HealthWidget = WidgetComp[0];


	if (this->GetController() == GetWorld()->GetFirstPlayerController())
	{
		TArray<UCameraComponent*> Camera;
		GetComponents<UCameraComponent>(Camera);
		MainCamera = Camera[0];
		ADSCamera = Camera[1];
		if (HealthWidget != nullptr)
		{
			HealthWidget->DestroyComponent();
		}
	}

}
// Called every frame
void AShooterCharacter::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

// Called to bind functionality to input
void AShooterCharacter::SetupPlayerInputComponent(UInputComponent* PlayerInputComponent)
{
	Super::SetupPlayerInputComponent(PlayerInputComponent);

	PlayerInputComponent->BindAxis(TEXT("MoveForward"), this, &AShooterCharacter::MoveForward);
	PlayerInputComponent->BindAxis(TEXT("LookUp"), this, &APawn::AddControllerPitchInput);
	PlayerInputComponent->BindAxis(TEXT("MoveRight"), this, &AShooterCharacter::MoveRight);
	PlayerInputComponent->BindAxis(TEXT("LookRight"), this, &APawn::AddControllerYawInput);
	PlayerInputComponent->BindAxis(TEXT("LookUpRate"), this, &AShooterCharacter::LookUpRate);
	PlayerInputComponent->BindAxis(TEXT("LookRightRate"), this, &AShooterCharacter::LookRightRate);
	PlayerInputComponent->BindAction(TEXT("Jump"), EInputEvent::IE_Pressed, this, &ACharacter::Jump);
	PlayerInputComponent->BindAction(TEXT("Shoot"), EInputEvent::IE_Pressed, this, &AShooterCharacter::Shoot);
	PlayerInputComponent->BindAction(TEXT("Shoot"), EInputEvent::IE_Released, this, &AShooterCharacter::Release);
	PlayerInputComponent->BindAction(TEXT("ReloadGun"), EInputEvent::IE_Pressed, this, &AShooterCharacter::ReloadGun);
	PlayerInputComponent->BindAction(TEXT("SwitchGun"), EInputEvent::IE_Pressed, this, &AShooterCharacter::SwitchGun);
	PlayerInputComponent->BindAction(TEXT("Melee"), EInputEvent::IE_Pressed, this, &AShooterCharacter::MeleeAttack);
	PlayerInputComponent->BindAction(TEXT("ZoomIn"), EInputEvent::IE_Pressed, this, &AShooterCharacter::ZoomIn);
	PlayerInputComponent->BindAction(TEXT("ZoomOut"), EInputEvent::IE_Released, this, &AShooterCharacter::ZoomOut);
	PlayerInputComponent->BindAction(TEXT("Interact"), EInputEvent::IE_Pressed, this, &AShooterCharacter::Interact);
}

void AShooterCharacter::MoveForward(float AxisValue)
{
	AddMovementInput(GetActorForwardVector() * AxisValue);
}

void AShooterCharacter::MoveRight(float AxisValue)
{
	AddMovementInput(GetActorRightVector() * AxisValue);
}

void AShooterCharacter::LookUpRate(float AxisValue)
{
	AddControllerPitchInput(AxisValue * RotationRate * GetWorld()->GetDeltaSeconds());
}

void AShooterCharacter::LookRightRate(float AxisValue)
{
	AddControllerYawInput(AxisValue * RotationRate * GetWorld()->GetDeltaSeconds());
}

void AShooterCharacter::Shoot() 
{
	ActiveGun->PullTrigger();
}

void AShooterCharacter::Release()
{
	ActiveGun->ReleaseTrigger();
}
void AShooterCharacter::ReloadGun()
{
	ActiveGun->Reload();
}

float AShooterCharacter::TakeDamage(float DamageAmount, struct FDamageEvent const& DamageEvent, class AController* Eventinstigator, AActor* DamageCauser) 
{
	float DamageToApply = Super::TakeDamage(DamageAmount, DamageEvent, Eventinstigator, DamageCauser);
	DamageToApply = FMath::Min(Health, DamageToApply);
	Health -= DamageToApply;	
	if (IsDead())
	{
		ASimpleShooterGameModeBase* GameMode = GetWorld()->GetAuthGameMode<ASimpleShooterGameModeBase>();
		if (GameMode != nullptr)
		{
			GameMode->PawnKilled(this);
		}
		DetachFromControllerPendingDestroy();
		GetCapsuleComponent()->SetCollisionEnabled(ECollisionEnabled::NoCollision); 
		
		
		if (HealthWidget != nullptr)
		{
			HealthWidget->DestroyComponent();
		}
	
	}
	return DamageToApply;
}

bool AShooterCharacter::IsDead() const
{
	return Health <= 0;
}

float AShooterCharacter::GetHealthPercent() const
{
	return Health / MaxHealth;
}

AGun* AShooterCharacter::GetActiveGun()
{
	return ActiveGun;
}

AGun* AShooterCharacter::GetHolsteredGun()
{
	return HolsteredGun;
}
void AShooterCharacter::SwitchGun()
{
	AGun* Temp = HolsteredGun;
	HolsteredGun = ActiveGun;
	ActiveGun = Temp; 	

	ActiveGun->SetActorHiddenInGame(false);
	HolsteredGun->SetActorHiddenInGame(true);
}

FName AShooterCharacter::GetCritSpot()
{
	return CritSpot;
}

void AShooterCharacter::MeleeAttack()
{
	SetIsAttacking(true);
}

bool AShooterCharacter::GetIsAttacking()
{
	return bIsAttacking;
}

void AShooterCharacter::SetIsAttacking(bool State)
{
	bIsAttacking = State; 
}

void AShooterCharacter::ZoomIn()
{
	MainCamera->SetActive(false);
	ADSCamera->SetActive(true);
	
}
void AShooterCharacter::ZoomOut()
{
	ADSCamera->SetActive(false);
	MainCamera->SetActive(true);
	
}


void AShooterCharacter::OnOverlapBegin(class UPrimitiveComponent* OverlappedComp, class AActor* OtherActor, class UPrimitiveComponent* OtherComp, int32 OtherBodyIndex, bool bFromSweep, const FHitResult& SweepResult)
{
	if(OtherActor != nullptr && Cast<AConsoleTrigger>(OtherActor) )
	{ 
		Console = Cast<AConsoleTrigger>(OtherActor);
	}
}
void AShooterCharacter::OnOverlapEnd(class UPrimitiveComponent* OverlappedComp, class AActor* OtherActor, class UPrimitiveComponent* OtherComp, int32 OtherBodyIndex)
{
	Console = nullptr;
}

void AShooterCharacter::Interact()
{
	if (Console != nullptr)
	{
		Console->OpenDoor(); 
	}
}