Starting from the end of the GameDev.tv course project of the same name I implemented more advanced systems. 
Initial project can be seen at https://gitlab.com/GameDevTV/UnrealCourse/SimpleShooter. 

Current Implementations:
    -Created more advanced AI
    -Added another enemy type
    -Created an automatic weapon
    -Created weapon switching mechanics
    -Created weapon reload mechanics
    -Added more UI elements to the HUD
    -Created Melee Combat Mechanics
    -Added interactable door mechanics
    -Created a Boss arena
